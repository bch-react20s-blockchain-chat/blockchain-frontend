import React from "react";
import { Form } from "react-bootstrap";

export default function MessageInput({
  handleMessageInput,
  handleMessageSend,
  className,
}) {
  /**
   * Handles form submit on enter. Shift-enter can still be used for a newline input.
   * @param {*} e
   */
  const handleEnter = (e) => {
    // which = 13 means the enter key
    if (e.which === 13 && !e.shiftKey) {
      e.preventDefault();
      handleMessageSend();
      e.target.closest("form").reset();
    }
  };

  return (
    <Form className={className}>
      <Form.Group
        controlId="exampleForm.ControlTextarea1"
        onChange={handleMessageInput}
        onKeyPress={handleEnter}
        className="mb-0"
      >
        <Form.Control
          as="textarea"
          rows={2}
          placeholder="Type a new message..."
          style={{ resize: "none" }}
        />
      </Form.Group>
    </Form>
  );
}
