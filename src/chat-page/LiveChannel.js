import React from "react";
import { Accordion, Badge } from "react-bootstrap";

export default function LiveChannel({
  channelName,
  toggleChannelList,
  children,
}) {
  return (
    <Accordion defaultActiveKey="0">
      <Accordion.Toggle
        as={Badge}
        pill
        variant="dark"
        eventKey="0"
        className="w-100 py-1  text-truncate actionable d-inline-flex"
        style={{ fontSize: "15px" }}
      >
        <Badge
          pill
          variant="light"
          className="w-100 px-5 py-1 text-truncate actionable"
          style={{ fontSize: "15px" }}
          onClick={toggleChannelList}
        >
          #{channelName}
        </Badge>
      </Accordion.Toggle>
      {children}
    </Accordion>
  );
}
