import React from "react";
import { Form } from "react-bootstrap";

export default function MessageSearch({ setMessageQuery }) {
  const handleEnter = (e) => {
    //Handles form submit on enter
    if (e.which === 13) {
      e.preventDefault();
      e.target.closest("form").reset();
      setMessageQuery("");
    }
  };

  return (
    <Form className="w-100">
      <Form.Group
        onChange={(e) => setMessageQuery(e.target.value)}
        onKeyPress={handleEnter}
      >
        <Form.Control
          className=" h-25 bg-light border rounded-pill"
          type="text"
          placeholder="Search Messages"
        />
      </Form.Group>
    </Form>
  );
}
