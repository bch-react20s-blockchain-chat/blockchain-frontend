import React, { useEffect, useRef } from "react";
import { Spinner } from "react-bootstrap";
import MessageCard from "./MessageCard";

export default function MessageList({ messages, messageQuery }) {
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  return (
    <>
      {messages.length ? (
        (messageQuery
          ? messages.filter((item) =>
              item.content
                ? item.content
                    .toLowerCase()
                    .includes(messageQuery.toLowerCase())
                : true
            )
          : messages
        ).map((msg, i) =>
          msg.loading ? (
            <Spinner key={-1} animation="border" />
          ) : (
            <MessageCard
              key={i}
              userId={msg.userId}
              content={msg.content}
              timestamp={msg.timestamp}
              fromMe={msg.fromMe}
            />
          )
        )
      ) : (
        <>Nothing here yet...</>
      )}
      <div ref={messagesEndRef} />
    </>
  );
}
