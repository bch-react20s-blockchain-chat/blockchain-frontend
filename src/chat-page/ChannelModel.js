import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import Modal from "react-bootstrap/Modal";

function ChannelAddModal({ addNewChannel, closeModal, modelIsOpen }) {
  const [show, setShow] = useState(false);
  const [showName, setShowName] = useState(true);
  const [channelName, setChannelName] = useState("");
  const [description, setDescription] = useState("");
  const [onSmallScreen, setOnSmallScreen] = useState(false);

  //Screen size check for adjusting Modal size
  useEffect(() => {
    checkScreenSize();
    window.addEventListener("resize", checkScreenSize);
  }, []);
  const checkScreenSize = () => {
    setOnSmallScreen(window.innerWidth < 600);
  };

  const handleNext = (e) => {
    if (channelName.length > 0) {
      if (channelName.length < 31) {
        setShowName(false);

        e.target.closest("form").reset();
      } else {
        alert("Channel Name input exceeded the character limit of 30 !!!");
      }
    } else {
      alert("Channel Name can not be empty  !!!");
    }
  };
  const handleClose = (e) => {
    addNewChannel(channelName, description);
    setChannelName("");
    setDescription("");
    e.target.closest("form").reset();
    setShowName(true);
    closeModal();
  };

  return (
    <div>
      <Modal
        show={modelIsOpen}
        onHide={() => closeModal()}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Adding your New Channel
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="p-3 mb-2   bg-secondary text-white ">
            <Form className="px-1 mb-2 mt-2" onSubmit={handleNext}>
              {showName ? (
                <Form.Group
                  className="px-1 my-1  "
                  controlId="exampleForm.ControlTextarea1"
                  onChange={(e) => setChannelName(e.target.value)}
                >
                  <label htmlFor="floatingInput">Channel Name</label>
                  <Form.Control
                    className="mb-4 text-dark "
                    as="textarea"
                    rows={2}
                    placeholder="Enter channel Name...[0-30 characters]"
                  />
                  <Button
                    className="mt-1 w-100 px-3 "
                    variant="success outline-success"
                    size="lg"
                    onClick={handleNext}
                  >
                    Next
                  </Button>
                </Form.Group>
              ) : (
                <Form.Group
                  controlId="exampleForm.ControlTextarea1"
                  onChange={(e) => setDescription(e.target.value)}
                  className="px-1 my-1"
                >
                  <label htmlFor="floatingInput">Channel Description</label>
                  <Form.Control
                    className="mb-4 text-dark"
                    as="textarea"
                    rows={2}
                    placeholder="Enter Channel Description..."
                  />
                  <Button
                    className="mt-1 w-100 px-3 "
                    variant="success outline-success"
                    size="lg"
                    onClick={handleClose}
                  >
                    Add Channel
                  </Button>
                </Form.Group>
              )}
            </Form>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ChannelAddModal;
