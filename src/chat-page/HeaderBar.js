import React from "react";
import { Navbar, Container, Col } from "react-bootstrap";

import UserCard from "./UserCard";
import logo from "./images/logo.svg";
import LiveChannel from "./LiveChannel";

export default function HeaderBar({
  user,
  channelName,
  toggleChannelList,
  children,
}) {
  return (
    <Navbar bg="dark" variant="dark" className="position-relative">
      <Container
        fluid
        as={Navbar.Brand}
        className="w-100 px-3 mr-0"
      >
        <Col xs={2}>
          <img
            alt=""
            src={logo}
            className="d-inline-block  align-top mr-1"
            href="#home"
          />
          <span className="mr-1 actionable d-none d-sm-none d-md-inline-block d-lg-inline-block">
            Blockchain Chat
          </span>
        </Col>
        <Col xs={7}>
          <LiveChannel
            channelName={channelName}
            toggleChannelList={toggleChannelList}
          >
            {children}
          </LiveChannel>
        </Col>
        <Col xs={3}>
          <UserCard userName={user.userName} userTitle={user.userTitle} />
        </Col>
      </Container>
    </Navbar>
  );
}
