import React from "react";
import renderer from "react-test-renderer";
import ChannelCard from "../ChannelCard";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <ChannelCard
        channelName={"Test channel"}
        channelDescription={"Description for the test channel."}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
