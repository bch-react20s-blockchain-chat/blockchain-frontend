import React from "react";
import renderer from "react-test-renderer";
import UserCard from "../UserCard";

it("renders correctly", () => {
  const tree = renderer
    .create(<UserCard userName="Test user" userTitle="Test title" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
