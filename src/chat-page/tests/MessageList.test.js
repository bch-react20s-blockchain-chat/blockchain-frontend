import React from "react";
import renderer from "react-test-renderer";
import MessageList from "../MessageList";

const messages = [
  {
    _id: "6059abff9bd4455248ea6c16",
    channelId: 1,
    content: "Welcome to the blockchain chat!",
    timestamp: "2021-03-23T08:50:54.309Z",
    userId: "Genesis",
    __v: 0,
  },
  {
    _id: "6059abff9bd4455248ea6c16",
    channelId: 1,
    content: "Welcome to the blockchain chat!",
    timestamp: "2021-03-23T08:50:54.309Z",
    userId: "Genesis",
    __v: 0,
  },
  {
    _id: "6059abff9bd4455248ea6c16",
    channelId: 1,
    content: "Welcome to the blockchain chat!",
    timestamp: "2021-03-23T08:50:54.309Z",
    userId: "Genesis",
    __v: 0,
  },
];

it("renders correctly", () => {
  const tree = renderer.create(<MessageList messages={messages} />).toJSON();
  expect(tree).toMatchSnapshot();
});
