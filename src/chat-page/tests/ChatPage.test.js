import React from "react";
import renderer from "react-test-renderer";
import ChatPage from "../ChatPage";

const userId = "Test user";
const messages = [
  {
    _id: "6059abff9bd4455248ea6c16",
    channelId: 1,
    content: "Welcome to the blockchain chat!",
    timestamp: "2021-03-23T08:50:54.309Z",
    userId: "Genesis",
    __v: 0,
  },
  {
    _id: "6059abff9bd4455248ea6c16",
    channelId: 1,
    content: "Welcome to the blockchain chat!",
    timestamp: "2021-03-23T08:50:54.309Z",
    userId: "Genesis",
    __v: 0,
  },
  {
    _id: "6059abff9bd4455248ea6c16",
    channelId: 1,
    content: "Welcome to the blockchain chat!",
    timestamp: "2021-03-23T08:50:54.309Z",
    userId: "Genesis",
    __v: 0,
  },
];
const currentChannel = {
  channelDescription: "Global root channel of the blockchain chat",
  channelId: 1,
  channelName: "Global",
};
const channels = [
  {
    channelDescription: "Global root channel of the blockchain chat",
    channelId: 1,
    channelName: "Global",
  },
  {
    channelDescription: "This is a another channel",
    channelId: 2,
    channelName: "another channel",
  },
];
const handleMessageInput = () => null;
const handleMessageSend = () => null;
const handleChannelChange = () => null;
const handleNewChannel = () => null;

it("renders correctly", () => {
  const tree = renderer
    .create(
      <ChatPage
        userId={userId}
        messages={messages}
        currentChannel={currentChannel}
        channels={channels}
        handleMessageInput={handleMessageInput}
        handleMessageSend={handleMessageSend}
        handleChannelChange={handleChannelChange}
        handleNewChannel={handleNewChannel}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
