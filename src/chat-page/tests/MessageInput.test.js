import React from "react";
import renderer from "react-test-renderer";
import MessageInput from "../MessageInput";

const handleMessageInput = () => null;
const handleMessageSend = () => null;

it("renders correctly", () => {
  const tree = renderer
    .create(
      <MessageInput
        handleMessageInput={handleMessageInput}
        handleMessageSend={handleMessageSend}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
