import React from "react";
import renderer from "react-test-renderer";
import MessageCard from "../MessageCard";

Date.now = jest.fn(() => 1482363367071);
const timestamp = Date.now();

it("renders correctly", () => {
  const tree = renderer
    .create(
      <MessageCard
        userId="Test user"
        content="Test message content"
        timestamp={timestamp}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
