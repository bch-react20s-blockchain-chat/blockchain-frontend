import React from "react";
import renderer from "react-test-renderer";
import HeaderBar from "../HeaderBar";

it("renders correctly", () => {
  const user = {
    userName: "Test user",
    userTitle: "Test title",
  };
  const tree = renderer
    .create(
      <HeaderBar
        user={user}
        channelName="Test channel"
        toggleChannelList={() => null}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
