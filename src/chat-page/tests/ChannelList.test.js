import React from "react";
import renderer from "react-test-renderer";
import ChannelList from "../ChannelList";

const currentChannelId = 1;
const currentChannelName = "Global";
const channels = [
  {
    channelDescription: "Global root channel of the blockchain chat",
    channelId: 1,
    channelName: "Global",
  },
  {
    channelDescription: "This is a another channel",
    channelId: 2,
    channelName: "another channel",
  },
];
const handleChannelChange = () => null;
const handleNewChannel = () => null;

it("renders correctly", () => {
  const tree = renderer
    .create(
      <ChannelList
        currentChannelId={currentChannelId}
        currentChannelName={currentChannelName}
        channels={channels}
        handleChannelChange={handleChannelChange}
        handleNewChannel={handleNewChannel}
      />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
