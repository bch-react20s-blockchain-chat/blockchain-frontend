import React from "react";

export default function UserCard({ userName, userTitle }) {
  return (
    <div className="d-flex align-items-center justify-content-end">
      <img
        className="rounded-circle user-img"
        alt="100x100"
        src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg"
        data-holder-rendered="true"
        height="50"
      ></img>
      <div className="ml-2 d-none d-sm-flex flex-column">
        <small>
          <b>{userName}</b>
        </small>
        <small>{userTitle}</small>
      </div>
    </div>
  );
}
