import { useState } from "react";
import { Container, Row } from "react-bootstrap";
import HeaderBar from "./HeaderBar";

import ChannelList from "./ChannelList";
import MessageInput from "./MessageInput";
import MessageList from "./MessageList";
import MessageSearch from "./MessageSearch";

function ChatPage({
  userId,
  messages,
  currentChannel,
  channels,
  handleMessageInput,
  handleMessageSend,
  handleChannelChange,
  handleNewChannel,
  setMessageQuery,
  messageQuery,
}) {
  const user = {
    userName: userId,
    userTitle: "User Job Title", //FIXME
  };

  const [showChannelList, setShowChannelList] = useState(false);

  const toggleChannelList = () => {
    setShowChannelList(!showChannelList);
  };

  return (
    <Container fluid className="p-0 vh-100">
      <Row as={"header"} xs={1} className="sticky-top">
        <HeaderBar
          user={user}
          channelName={currentChannel.channelName}
          toggleChannelList={toggleChannelList}
        >
          <ChannelList
            className={showChannelList ? "d-block" : "d-none"}
            currentChannelId={currentChannel.channelId}
            currentChannelName={currentChannel.channelName}
            channels={channels}
            handleChannelChange={handleChannelChange}
            handleNewChannel={handleNewChannel}
            toggleChannelList={toggleChannelList}
          />
        </HeaderBar>
      </Row>
      <Row
        as={"main"}
        xs={12}
        className="mx-3 py-3 d-flex flex-column align-items-center"
        style={{ marginBottom: "76px" }}
      >
        <MessageSearch setMessageQuery={setMessageQuery} />
        <MessageList messages={messages} messageQuery={messageQuery} />
      </Row>
      <Row
        as={"footer"}
        className="px-3 pb-3 blur top position-fixed fixed-bottom"
        style={{ background: "white" }}
      >
        <MessageInput
          handleMessageInput={handleMessageInput}
          handleMessageSend={handleMessageSend}
          className="w-100 px-3"
        />
      </Row>
    </Container>
  );
}

export default ChatPage;
