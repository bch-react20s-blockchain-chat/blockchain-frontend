import React from "react";
import { Card } from "react-bootstrap";

export default function MessageCard({ userId, content, timestamp, fromMe }) {
  return (
    <div
      className="w-100 mb-2 d-flex"
      style={
        !fromMe ? { flexDirection: "row-reverse" } : { flexDirection: "row" }
      }
    >
      <img
        className="rounded-circle flex-shrink-0 mt-1"
        alt="user face"
        src="https://mdbootstrap.com/img/Photos/Avatars/img%20(30).jpg"
        data-holder-rendered="true"
        height="50"
      />
      <img
        className="rounded-circle"
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Ski_trail_rating_symbol-green_circle.svg/600px-Ski_trail_rating_symbol-green_circle.svg.png"
        alt="user-status"
        height="20"
        style={{
          transform: !fromMe ? "translate(80%, 170%)" : "translate(-80%, 170%)",
          border: "1px solid white",
        }}
      />
      <Card className="w-100">
        <Card.Body>
          <Card.Title className="mb-1">{userId}</Card.Title>
          <Card.Subtitle className="text-muted mb-1">
            {new Date(timestamp).toUTCString()}
          </Card.Subtitle>
          <Card.Text>{content}</Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
}
