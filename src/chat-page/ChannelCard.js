import React from "react";

export default function ChannelCard({ channelName, channelDescription }) {
  return (
    <div>
      <b>#{channelName}</b>
      <br />
      <span className="text-truncate">{channelDescription}</span>
    </div>
  );
}
