import React from "react";
import { Accordion, Badge, Spinner } from "react-bootstrap";

export default function ChannelList({
  currentChannelId,
  currentChannelName,
  channels,
  handleChannelChange,
  handleNewChannel,
  toggleChannelList,
}) {
  const handleClick = (channelId) => {
    handleChannelChange(channelId);
    toggleChannelList();
  };

  return (
    <Accordion.Collapse eventKey="0" className="mt-2 w-100">
      <div className="d-flex flex-column w-100">
        <Badge
          pill
          variant="dark"
          onClick={handleNewChannel}
          className="actionable w-100 mb-0 text-truncate"
        >
          Add a new one...
        </Badge>
        {channels.map((channel) =>
          channel.channelId !== currentChannelId ? (
            channel.loading ? (
              <Spinner
                animation="border"
                key={-1}
                size="sm"
                className="mb-1 align-self-center"
              ></Spinner>
            ) : (
              <Badge
                pill
                variant="light"
                key={channel.channelId}
                onClick={() => handleClick(channel.channelId)}
                className="w-100 mb-1 text-truncate actionable"
              >
                #{channel.channelName}
              </Badge>
            )
          ) : (
            <Badge
              pill
              variant="success"
              key={currentChannelId}
              className="w-100 mb-1 text-truncate"
            >
              #{currentChannelName}
            </Badge>
          )
        )}
      </div>
    </Accordion.Collapse>
  );
}
