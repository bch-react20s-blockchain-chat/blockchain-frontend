import React, { useState } from "react";
import { Button, Card, Form, Container, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import auth from "../services/auth.service";
import background from "./images/background.jpeg";
import Modal from "react-bootstrap/Modal";

const Login = ({ setIsAuthenticated, setUserName }) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [show, setShow] = useState(false);
  const [error, setError] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    auth
      .login(email, password)
      .then((res) => {
        setIsAuthenticated(true);
        setUserName(res.data.username);
      })
      .catch((error) => {
        setShow(true);
        console.log(error);
        setError(error);
      });
  };

  return (
    <div style={{ backgroundImage: `url(${background})` }}>
      <Container className="d-flex align-items-center justify-content-center min-vh-100">
        <Card style={{ width: "30rem" }}>
          <Col className="pt-5 pr-5 pl-5 pb-3 text-center" sm={12}>
            <Form
              className="d-flex flex-column justify-content-center"
              onSubmit={handleSubmit}
            >
              <h4 className="mb-3 mx-auto">Block Chat</h4>
              <Form.Group controlId="formBasicEmail">
                {/* First form selection (email) */}
                <Form.Control
                  type="email"
                  name="email"
                  placeholder="someone@somewhere.com"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Form.Group>
              {/* Second form selection (password) */}
              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  name="password"
                  placeholder="••••••••••"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                {" "}
                Submit
              </Button>
            </Form>
            <Container className="mt-3">
              <p className="m-0">
                Forgot your <a href="/">Password?</a>
              </p>
              <Link to="/register" className="m-0">
                Create an account? Sign Up
              </Link>
              <p>
                Sign in as <a href="/">Demo User</a>
              </p>
            </Container>
          </Col>
        </Card>
        {show ? (
          <Modal
            show={show}
            onHide={() => setShow(false)}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-custom-modal-styling-title">
                Error
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <p>{error.message}</p>
            </Modal.Body>
          </Modal>
        ) : (
          <> </>
        )}
      </Container>
    </div>
  );
};
export default Login;
