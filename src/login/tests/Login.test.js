import React from "react";
import renderer from "react-test-renderer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "../Login";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <Router>
        <Switch>
          <Route exact path="/">
            <Login />
          </Route>
          <Route path="/register" />
        </Switch>
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
