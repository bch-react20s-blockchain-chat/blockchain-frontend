import React from "react";
import renderer from "react-test-renderer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SignUp from "../Signup";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <Router>
        <Switch>
          <Route exact path="/">
            <SignUp />
          </Route>
        </Switch>
      </Router>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
