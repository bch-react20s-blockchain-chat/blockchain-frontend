import React, { useState } from "react";
import { Button, Card, Form, Container, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import auth from "../services/auth.service";
import background from "./images/background.jpeg";
import Modal from "react-bootstrap/Modal";

const SignUp = ({ setUserName, setIsAuthenticated }) => {
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [emailConfirm, setEmailConfirm] = useState();
  const [password, setPassword] = useState();
  const [passwordConfirm, setPasswordConfirm] = useState();
  const [show, setShow] = useState(false);
  const [error, setError] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (email === emailConfirm && password === passwordConfirm) {
      try {
        await auth.register(name, email, password);
        setUserName(name);
        setIsAuthenticated(true);
      } catch (error) {
        setShow(true);
        setError(error.message);
      }
    } else if (email !== emailConfirm && password === passwordConfirm) {
      setShow(true);
      setError("Emails do not match");
    } else if (password !== passwordConfirm && email === emailConfirm) {
      setShow(true);
      setError("Passwords do not match");
    } else {
      setShow(true);
      setError("both passwords and emails do not match");
    }
  };

  return (
    <div style={{ backgroundImage: `url(${background})` }}>
      <Container className="d-flex align-items-center justify-content-center min-vh-100">
        <Card style={{ width: "30rem" }}>
          <Col className="pt-5 pr-5 pl-5 pb-3 text-center" sm={12}>
            <Form
              className="d-flex flex-column justify-content-center"
              onSubmit={handleSubmit}
            >
              <h4 className="mb-3 mx-auto">Block Chat</h4>
              <p className="mb-3 mx-auto">Sign Up</p>
              <Form.Group controlId="formBasicName">
                {/* First form selection (name) */}
                <Form.Control
                  type="text"
                  name="name"
                  placeholder="Desired Username"
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                {/* First form selection (email) */}
                <Form.Control
                  type="email"
                  name="email"
                  placeholder="someone@somewhere.com"
                  onChange={(e) => setEmail(e.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="formEmailChecker">
                {/* Second form selection (confirmEmail) */}
                <Form.Control
                  type="email"
                  name="emailConfirm"
                  placeholder="Confirm email"
                  onChange={(e) => setEmailConfirm(e.target.value)}
                />
              </Form.Group>
              {/* Third form selection (password) */}
              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  name="password"
                  placeholder="••••••••••"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </Form.Group>
              {/* Fourth form selection (passwordChecker) */}
              <Form.Group controlId="formPasswordChecker">
                <Form.Control
                  type="password"
                  name="passwordConfirm"
                  placeholder="••••••••••"
                  onChange={(e) => setPasswordConfirm(e.target.value)}
                />
              </Form.Group>
              <Button variant="primary" type="submit">
                {" "}
                Submit
              </Button>
            </Form>
            <Container className="mt-3">
              <p className="m-0">
                Forgot your <a href="/">Password?</a>
              </p>
              <Link className="m-0" to="/">
                Already have an Account? Sign In
              </Link>
              <p>
                Sign in as <a href="/">Demo User</a>
              </p>
            </Container>
          </Col>
        </Card>
        {show ? (
          <Modal
            show={show}
            onHide={() => setShow(false)}
            dialogClassName="modal-90w"
            aria-labelledby="example-custom-modal-styling-title"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-custom-modal-styling-title">
                Error
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>{error ? <p>{error}</p> : <></>}</Modal.Body>
          </Modal>
        ) : (
          <> </>
        )}
      </Container>
    </div>
  );
};

export default SignUp;
