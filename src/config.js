import dotenv from 'dotenv';
dotenv.config();

export const WS = {
  wsUri: process.env.REACT_APP_WSURI || 'http://localhost:4000'
};

export const API = {
  apiUri: process.env.REACT_APP_APIURI || "http://localhost:4000",
};
