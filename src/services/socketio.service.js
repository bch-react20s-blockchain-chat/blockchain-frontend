import { io } from "socket.io-client";
import { WS } from "../config";

class SocketIOService {
  socket;

  initiateSocketConnection = (cb) => {
    console.log(`Connecting socket...`);
    this.socket = io(WS.wsUri);
    console.log("Socket connected!");
  };

  disconnectSocket = () => {
    //TODO
  };

  subscribeToMessages = (cb) => {
    this.socket.on("messages", (msg) => cb(null, msg));
  };

  subscribeToMessage = (cb) => {
    this.socket.on("message", (msg) => cb(null, msg));
  };

  subscribeToChannels = (cb) => {
    this.socket.on("channels", (channels) => cb(null, channels));
  };

  sendMessage = (msg) => {
    this.socket.emit("message", msg);
  };

  changeChannel = (id) => {
    this.socket.emit("channel change", id);
  };

  addChannel = (channel) => {
    this.socket.emit("channel add", channel);
  };
}

export default new SocketIOService();
