import axios from "axios";
import { API } from "../config";

class AuthService {
  api;

  constructor() {
    this.api = axios.create({ baseURL: API.apiUri });
  }

  fetchUser = async () => this.api.get("/user");

  register = async (name, email, password) =>
    this.api.post("/register", {
      name: name,
      email: email,
      password: password,
    });

  login = async (email, password) =>
    this.api.post("/login", {
      email: email,
      password: password,
    });
}

export default new AuthService();
