import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import io from "./services/socketio.service";
import auth from "./services/auth.service";
import Login from "./login/Login";
import SignUp from "./login/Signup";
import ChatPage from "./chat-page/ChatPage";
import ChannelAddModal from "./chat-page/ChannelModel";
import "./App.scss";

function App() {
  const [initialized, setInitialized] = useState(false);
  const [userName, setUserName] = useState(""); //FIXME: Length of the username should be capped to some 20? or so characters.
  const [messages, setMessages] = useState([]);
  const [messageInput, setMessageInput] = useState("");
  const [channels, setChannels] = useState([{ loading: true }]);
  const [currentChannelId, setCurrentChannelId] = useState(0);
  const [loadMessages, setLoadMessages] = useState(true);
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [messageQuery, setMessageQuery] = useState("");
  const [modelIsOpen, setModelIsOpen] = useState(false);
  useEffect(() => {
    //FIXME: Should read jwt cookie from browser, use eg. "react-cookie", and only then try this
    if (false)
      auth.fetchUser().then((res) => {
        setUserName(res.userName);
        setIsAuthenticated(true);
      });

    if (userName && !initialized) {
      // Initiate socket.io connection
      io.initiateSocketConnection();

      // Subscribe to the list of all channels
      io.subscribeToChannels((err, data) => {
        setChannels([...data]);
      });

      // Subscribe to messages in current channel. Current channel is maintained server-side.
      io.subscribeToMessages((err, data) => {
        setMessages(data);

        // If we received something, ensure correct current channel is highlighted.
        if (data.length) setCurrentChannelId(data[0].channelId);

        // Load has been completed whenever the socket sets the messages array.
        setLoadMessages(false);
      });

      // Subscribe to new messages in current channel. Current channel is maintained server-side.
      io.subscribeToMessage((err, data) => {
        setMessages((messages) => [...messages, data]);
      });

      setInitialized(true);
    }

    return () => io.disconnectSocket();
  }, [userName, initialized, isAuthenticated]);

  const handleMessageSend = () => {
    if (messageInput) {
      const newMsg = {
        channelId: currentChannelId,
        userId: userName, //FIXME: should use id instead of name
        content: messageInput,
        timestamp: Date.now(),
      };
      io.sendMessage(newMsg);
      //FIXME: error handling if the message could not be added to the blockchain?
      setMessages((messages) => [...messages, newMsg]);
      setMessageInput("");
    }
  };

  const handleMessageInput = (e) => setMessageInput(e.target.value);

  const handleChannelChange = (id) => {
    setCurrentChannelId(id);
    io.changeChannel(id);
    setLoadMessages(true); // Backend will send the messages of the selected channel.
  };

  const handleNewChannel = () => {
    setModelIsOpen(true);
  };

  const addNewChannel = (channelName, description) => {
    setChannels([...channels, { loading: true }]);
    io.addChannel({
      channelName,
      channelDescription: description,
    });
    setModelIsOpen(false);
  };
  const closeModal = (e) => {
    setModelIsOpen(false);
  };

  return isAuthenticated ? (
    <>
      <ChannelAddModal
        addNewChannel={addNewChannel}
        closeModal={closeModal}
        modelIsOpen={modelIsOpen}
      />
      <ChatPage
        userId={userName}
        messages={
          loadMessages
            ? [{ loading: true }]
            : messages.length
            ? messages.map((m) => ({ ...m, fromMe: m.userId === userName }))
            : messages
        } //HACKY way to tell new messages are being loaded
        currentChannel={
          channels.find((ch) => ch.channelId === currentChannelId) || {}
        }
        channels={channels}
        handleMessageInput={handleMessageInput}
        handleMessageSend={handleMessageSend}
        handleChannelChange={handleChannelChange}
        handleNewChannel={handleNewChannel}
        setMessageQuery={setMessageQuery}
        messageQuery={messageQuery}
      />
    </>
  ) : (
    <Router>
      <Switch>
        <Route exact path="/">
          <Login
            setIsAuthenticated={setIsAuthenticated}
            setUserName={setUserName}
          />
          ;
        </Route>
        <Route path="/register">
          <SignUp
            setUserName={setUserName}
            setIsAuthenticated={setIsAuthenticated}
          />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
